import React from 'react';

class CPFInput extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      result: null,
    };
  }

  handleCpfApi = (event) => {
    let cpfValue = event.target.value;
    fetch('http://api.devops.prod/validator/?cpf=' + cpfValue)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      this.setState({result: data.result});
    })
    .catch(console.log);
  }

  render() {
    return (
      <div>
        <input id="cpfValidatorInput" onKeyUp={this.handleCpfApi} />
        <p id="resultCpfValidator">{this.state.result ? 'Válido' : 'Inválido'}</p>
      </div>
    );
  }
}

export default CPFInput;
