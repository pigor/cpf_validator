import { render, screen } from '@testing-library/react';
import App from './App';

test('renders result element', () => {
  render(<App />);
  const resultElement = screen.getByText(/Inválido/i);
  expect(resultElement).toBeInTheDocument();
});
